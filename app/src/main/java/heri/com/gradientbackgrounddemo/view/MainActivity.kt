package heri.com.gradientbackgrounddemo.view

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.graphics.drawable.AnimationDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import heri.com.gradientbackgrounddemo.R

class MainActivity : AppCompatActivity() {

    lateinit var animationDrawable:AnimationDrawable
    lateinit var binding: ViewDataBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = DataBindingUtil.setContentView<ViewDataBinding>(this, R.layout.activity_main);
        setGradientBackground();
    }

    private fun setGradientBackground()
    {
        val constraintLayout = findViewById<ConstraintLayout>(R.id.root) as ConstraintLayout
        constraintLayout.setBackgroundResource(R.drawable.dark_gradient_anim);
        animationDrawable = constraintLayout.background as AnimationDrawable
        animationDrawable.setEnterFadeDuration(2000)
        animationDrawable.setExitFadeDuration(4000)
        animationDrawable.start()
    }

    override fun onPause() {
        animationDrawable.stop()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        animationDrawable.start()
    }
}
